<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class JokeController extends Controller
{
    /**
     * Generate an array of jokes to be display on a frontend using a third
     * party API.
     *
     * @param Request $request
     * @return void
     */
    public function generate(Request $request)
    {
        try {
            // call the api 
            $response = Http::get('http://api.icndb.com/jokes/random/' . $request->amount);
        } catch (Exception $e) {
            // if there's an error with the call return error message
            return [
                'status' => 'error',
                'message' => 'An unexpected error occured.'
            ];
        }

        // return successful response with jokes from API
        return [
            'success' => 'The jokes were generated successfully.',
            'jokes' => $response->json()["value"]
        ];
    }
}
