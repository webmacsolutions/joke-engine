<!doctype html>
<html lang="en">

<head>
    <title>Joke Engine - @yield('title')</title>

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app">
        <div class="container">
            @yield('content')
        </div>
    </div>

    <script src="/js/app.js"></script>
</body>

</html>