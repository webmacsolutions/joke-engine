# Joke Engine

This is a simple Joke Engine written in Laravel 8 which generates jokes using a third party API.

## Getting started

### Launch the starter project

_(Assuming you've [installed Laravel](https://laravel.com/docs/8.x/installation))_

Fork this repository, then clone your fork, and run this in your newly created directory:

```bash
composer install
```

Next you need to make a copy of the `.env.example` file and rename it to `.env` inside your project root.

Run the following command to generate your app key:

```
php artisan key:generate
```

Run the following command to install your javascript dependencies:

```
npm install
```

Run the following command to build frontend for development:

```
npm run dev
```

Run the following command to build frontend for production:

```
npm run production
```

Then start your server:

```
php artisan serve
```

Your Laravel starter project is now up and running!

## How would I go about improving this application?

-   Config out the API URLs into an array and include their expected key for
    the joke value e.g. I've use the first API which returns the jokes under
    the 'value' key.
-   Add some more custom SCSS styling to make the frontend a bit more appealing
-   Add a loading spinner rather than just spit out text saying 'Loading...'
-   Add more ways the user can interact with the API(s) e.g. Choose the type
    of category for the joke.
